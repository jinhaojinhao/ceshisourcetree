//
//  MainViewController.m
//  ceshiDemo
//
//  Created by guji on 2020/5/23.
//  Copyright © 2020 guji. All rights reserved.
//

#import "MainViewController.h"
#import "OneViewController.h"
@interface MainViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textfield;

@end

@implementation MainViewController
- (IBAction)pushAction:(id)sender {
    [self.navigationController pushViewController:[OneViewController new] animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title  = @"播放";
    self.view.backgroundColor = [UIColor whiteColor];
    self.textfield.tintColor = [UIColor redColor];
    // Do any additional setup after loading the view from its nib.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
