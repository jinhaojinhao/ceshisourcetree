//
//  AppDelegate.h
//  ceshiDemo
//
//  Created by guji on 2020/5/14.
//  Copyright © 2020 guji. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@end

