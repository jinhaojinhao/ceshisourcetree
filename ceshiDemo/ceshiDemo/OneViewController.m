//
//  OneViewController.m
//  ceshiDemo
//
//  Created by guji on 2020/5/23.
//  Copyright © 2020 guji. All rights reserved.
//

#import "OneViewController.h"
#import "TwoViewController.h"

@interface OneViewController ()

@end

@implementation OneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title  = @"播放详情";
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)push:(id)sender {
      [self.navigationController pushViewController:[TwoViewController new] animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
